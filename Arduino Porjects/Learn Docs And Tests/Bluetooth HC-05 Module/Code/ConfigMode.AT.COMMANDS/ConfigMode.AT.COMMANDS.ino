
#include <SoftwareSerial.h>

SoftwareSerial BTSerial(10,11); // RX | TX 10,11

void setup()
{
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  Serial.println("Config Mode:");
  //  BTSerial.begin(38400);  // HC-05 default speed in AT command more (FOR SLAVE - 38400 - FOR MATER - 9600 - DUE TO PI SETUP)
  BTSerial.begin(9600);  //custom

}

void loop()
{

  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available())
    Serial.write(BTSerial.read());

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available())
    BTSerial.write(Serial.read());
}
