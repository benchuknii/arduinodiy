#define trigPin 7
#define echoPin 6

void steup() {
  Serial.being(9600);
  pinMode(trigPin, OUTPUT);
}

void loop(){
  int duration, distance;
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(1000);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2)/29.1;
  Serial.print(distance);
  Serial.println(" cm");
  int pitch = map(distance,0,200,200,2093);
  tone(4, pitch);
  delay(500);
  noTone(4);
}
