/* Blink without Delay
 
 Turns on and off a light emitting diode(LED) connected to a digital  
 pin, without using the delay() function.  This means that other code
 can run at the same time without being interrupted by the LED code.
 
 The circuit:
 * LED attached from pin 13 to ground.
 * Note: on most Arduinos, there is already an LED on the board
 that's attached to pin 13, so no hardware is needed for this example.
 
 created 2005
 by David A. Mellis
 modified 8 Feb 2010
 by Paul Stoffregen
 modified 11 Nov 2013
 by Scott Fitzgerald
 
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/BlinkWithoutDelay
 */
#include <SoftwareSerial.h>

SoftwareSerial mySerial(0, 1); // RX, TX
// constants won't change. Used here to set a pin number :
const int ledPin =  10;      // the number of the LED pin

// Variables will change :
int ledState = LOW;             // ledState used to set the LED

// Generally, you shuould use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change :
const long interval = 1000;           // interval at which to blink (milliseconds)

void setup() {
  // set the digital pin as output:
  pinMode(ledPin, OUTPUT);      
  mySerial.begin(9600);
  mySerial.println("Serial is on: ");
  digitalWrite(ledPin, 255);
  
}

void loop()
{
   // only if there are bytes in the serial buffer execute the following code
  if(mySerial.available()) {    
    //inform that Arduino heard you saying something
    mySerial.print("Arduino heard you say: ");
    
    //keep reading and printing from serial untill there are bytes in the serial buffer
     while (mySerial.available()>0){
       mySerial.write(mySerial.read());
        //slows down the visualization in the terminal
      delay(10);
     digitalWrite(ledPin, HIGH);
     delay(10);
     digitalWrite(ledPin, LOW);
     delay(10);
     }
     
  }
  
 
}

