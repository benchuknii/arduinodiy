/**
 * An Mirf example which copies back the data it recives.
 *
 * Pins:
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 *
 */

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>
// Structure of our payload

//struct payload_t
//{
//  unsigned long main_command;//1 ac
//  unsigned long sub_command;//1=on, 0=off
//};
const int Radio_CSN = 10; // pinnenummer brukt for CSN på radio (ikke endre denne)
const int Radio_CE  = 9;  //
void setup() {
  Serial.begin(9600);

  /*
   * Set the SPI Driver.
   */
  Mirf.csnPin =  Radio_CSN;                                                                                                                                                          ; // kan endres etter behov. Kan utelates hvis default
  Mirf.cePin  = Radio_CE;
  Mirf.spi = &MirfHardwareSpi;

  /*
   * Setup pins / SPI.
   */

  Mirf.init();

  /*
   * Configure reciving address.
   */

  Mirf.setRADDR((byte *)"serv1");

  /*
   * Set the payload length to sizeof(unsigned long) the
   * return type of millis().
   *
   * NB: payload on client and server must be the same.
   */

  Mirf.payload = (sizeof(unsigned long) + sizeof(unsigned long));

  /*
   * Write channel and payload config then power up reciver.
   */

  Mirf.config();

  Serial.println("Listening...");
}

void loop() {
  /*
   * A buffer to store the data.
   */

  byte data[Mirf.payload];

  /*
   * If a packet has been recived.
   *
   * isSending also restores listening mode when it
   * transitions from true to false.
   */

  if (!Mirf.isSending() && Mirf.dataReady()) {
    Serial.println("Got packet");

    /*
     * Get load the packet into the buffer.
     */

    Mirf.getData(data);

    /*
     * Set the send address.
     */


    Mirf.setTADDR((byte *)"irir1");

    /*
     * Send the data back to the client.
     */
    unsigned long main_command = 1;
    unsigned long sub_command = 1;
    data[0] = (int)((main_command >> 24) & 0xFF) ;
    data[1] = (int)((main_command >> 16) & 0xFF) ;
    data[2] = (int)((main_command >> 8) & 0XFF);
    data[3] = (int)((main_command & 0XFF));
    data[4] = (int)((sub_command >> 24) & 0xFF) ;
    data[5] = (int)((sub_command >> 16) & 0xFF) ;
    data[6] = (int)((sub_command >> 8) & 0XFF);
    data[7] = (int)((sub_command & 0XFF));

    Mirf.send(data);

    /*
     * Wait untill sending has finished
     *
     * NB: isSending returns the chip to receving after returning true.
     */

    Serial.println("Reply sent.");
  }
}
