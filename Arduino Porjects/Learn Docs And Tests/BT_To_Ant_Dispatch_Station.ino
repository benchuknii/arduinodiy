
#include <SoftwareSerial.h>
#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>

// nRF24L01(+) radio attached using Getting Started board
RF24 radio(9, 10);//CE-9/CSN-10

// Network uses that radio
RF24Network network(radio);
SoftwareSerial BTSerial(6,7); // RX | TX
// Address of our node
const uint16_t this_node = 0;
const uint16_t other_node_ir_station_1 = 1;

// Structure of our payload
struct payload_t
{
  unsigned long main_command;//1 ac
  unsigned long sub_command;//1=on, 0=off
};

void setup()
{

  Serial.begin(57600);
  Serial.println("Dispatch station ready.");
  //BTSerial.begin(38400);  // HC-05 default speed in AT command more
  BTSerial.begin(9600);  //custom
  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 90, /*node address*/ this_node);
}
int i = 0;
void loop()
{
  // Pump the network regularly
  network.update();

  // Is there anything ready for us?
  while ( network.available() )
  {
    // If so, grab it and print it out
    RF24NetworkHeader header;
    payload_t payload;
    network.read(header, &payload, sizeof(payload));
    Serial.print("Dispatch station Received ack/status packet #");
    Serial.print(payload.main_command);
    Serial.print(" at ");
    Serial.println(payload.sub_command);
  }
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available())
  {
    Serial.write(BTSerial.read());
    //add code to check what was requested from serial server (PI station)
    
    //dispatch message to relevant station 
    payload_t payload = { 1, 1 };//ac on //1 is ac the second 1 is on-1/off-0
    RF24NetworkHeader header(/*to node*/ other_node_ir_station_1);
    bool ok = network.write(header,&payload,sizeof(payload));
    if (ok)
      Serial.println("dispatch ok.");
    else
      Serial.println("dispatch failed.");
  }
}
