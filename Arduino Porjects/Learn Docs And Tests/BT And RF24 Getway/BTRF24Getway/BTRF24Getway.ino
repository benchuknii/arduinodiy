#include <RF24Network.h>
#include <RF24Network_config.h>
#include <Sync.h>

/*
 Copyright (C) 2012 James Coliz, Jr. <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

/**
 * Simplest possible example of using RF24Network 
 *
 * TRANSMITTER NODE
 * Every 2 seconds, send a payload to the receiver node.
 */

#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>

#include <SoftwareSerial.h>
SoftwareSerial BTSerial(10, 11); // RX | TX

// nRF24L01(+) radio attached using Getting Started board 
RF24 radio(9,10);

// Network uses that radio
RF24Network network(radio);

//http://stackoverflow.com/questions/6669842/how-to-best-achieve-string-to-number-mapping-in-a-c-program
struct entry {
    char *str;
    int n;
};

struct entry dict[] = {
    "GETWAY_NODE", 0,
    "REMOTE1_NODE", 1,
    "LIGHT1_NODE", 2,
    "LIGHT2_NODE", 3
};
int MAX_MESSAGE_LENGTH = 51;//1 for null termination
char message[MAX_MESSAGE_LENGTH];
// Address of our node
//const uint16_t this_node = 0;

// Address of the other node
//const uint16_t other_node = 0;
//const uint16_t REMOTE1_NODE = 1;
//const uint16_t LIGHT1_NODE = 2;

// How often to send 'hello world to the other unit
const unsigned long interval = 2000; //ms

// When did we last send?
unsigned long last_sent;

// How many have we sent already
unsigned long packets_sent;

// Structure of our payload
struct payload_t
{
  unsigned long ms;
  unsigned long counter;

};

void setup(void)
{
  Serial.begin(57600);
  Serial.println("GETWAY_NODE Running...");
 
  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 90, /*node address*/ number_for_key("GETWAY_NODE"));
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  Serial.println("Serial BT to PI Server is Running...");
  BTSerial.begin(38400);  // HC-05 default speed in AT command more
  //BTSerial.begin(57600);  //custom
}

void loop(void)
{
  // Pump the network regularly
  network.update();

  if (BTSerial.available())
  {
     //check for known message from PI server
    string pi_command = BTSerial.read();
    //split PI COMMADN TO destination and command
    //find destination on the dict using numerForKey func
    //exmp: LIGHT1_NODE,ON
    //http://stackoverflow.com/questions/9210528/split-string-with-delimiters-in-c
    "GOTO TABLE OF COMMANDS AND SEARCH COMMAND"
    if(1)
    {
      Serial.print("Forward command...");
      strncpy(message, pi_command, 100);
      payload_t payload = { millis(), packets_sent++, message };
      RF24NetworkHeader header(/*to node*/ other_node);
      bool ok = network.write(header,&payload,sizeof(payload));
      if (ok)
        Serial.println("ok.");
      else
        Serial.println("failed.");
    }
  }

  // If it's time to send a message, send it!
  unsigned long now = millis();
  if ( now - last_sent >= interval  )
  {
    last_sent = now;

    Serial.print("Sending...");
    payload_t payload = { millis(), packets_sent++ };
    RF24NetworkHeader header(/*to node*/ other_node);
    bool ok = network.write(header,&payload,sizeof(payload));
    if (ok)
      Serial.println("ok.");
    else
      Serial.println("failed.");
      
       Serial.print("Sending2...");
    payload_t payload2 = { millis(), packets_sent++ };
    RF24NetworkHeader header2(/*to node*/ other_node2);
    bool ok2 = network.write(header2,&payload2,sizeof(payload2));
    if (ok2)
      Serial.println("ok2.");
    else
      Serial.println("failed2.");
  }
}


int number_for_key(char *key)
{
    int i = 0;
    char *name = dict[i].str;
    while (name) {
        if (strcmp(name, key) == 0)
            return dict[i].n;
        name = dict[++i].str;
    }
    return 0;
}
// vim:ai:cin:sts=2 sw=2 ft=cpp